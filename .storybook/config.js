// .storybook/config.js

import { configure } from "@storybook/react";

function loadStories() {
  require("../src/stories/Button.js");
  require("../src/stories/TextInput.js");
  // You can require as many stories as you need.
}

configure(loadStories, module);