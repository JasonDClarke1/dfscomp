import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Button from './Button';

configure({adapter: new Adapter()});

describe('<Button />', () => {
    let wrapper; 
    beforeEach(() => {
        wrapper = shallow(<Button/>);
    })

    it('should use label text as text content', () => {
        wrapper.setProps({label: "Hello"});
        expect(wrapper.text()).toBe("Hello");
    })
})

// https://airbnb.io/enzyme/docs/api/shallow.html