import React from "react";
import { storiesOf } from "@storybook/react";

import Button from "../features/common/Button/Button";

storiesOf("Button", module).add("with continue", () => (
    <Button label={`Continue`} />
  ));