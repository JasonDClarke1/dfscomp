import React from "react";
import { storiesOf } from "@storybook/react";

import Button from "../features/common/Button/Button";

storiesOf("TextInput", module).add("with text", () => (
    <Button label={`Continue`} />
  ));